FROM node:10-alpine AS instapack
RUN npm install -g instapack@7.1.2
COPY . /src
WORKDIR /src/EvanAutorestDemo
RUN ipack

FROM microsoft/dotnet:2.2-sdk-alpine AS build
COPY --from=instapack /src /src
WORKDIR /src/EvanAutorestDemo
RUN dotnet restore
RUN dotnet publish -c Release

FROM microsoft/dotnet:2.2-aspnetcore-runtime-alpine AS runtime
COPY --from=build /src/EvanAutorestDemo/bin/Release/netcoreapp2.2/publish /app
WORKDIR /app
ENTRYPOINT ["dotnet", "EvanAutorestDemo.dll"]