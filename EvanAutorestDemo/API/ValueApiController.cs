﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EvanAutorestDemo.API
{
    public class ValueApiRequestModel
    {
        [Required]
        [StringLength(255)]
        public string Name { set; get; }
    }

    [Route("api/[controller]")]
    [ApiController]
    public class ValueApiController : ControllerBase
    {
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        [HttpPost]
        public ActionResult<ValueApiRequestModel> Post([FromBody] [Required] ValueApiRequestModel value)
        {
            return value;
        }
    }
}
